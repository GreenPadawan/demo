﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PotentialFunction{
	public float A, B, n, m;
}

public class EnemyStateManager : MonoBehaviour {

    public Transform[] waypoints;
    public float sightRange = 20f;
    public Transform origin;
    public float turningSpeed;
    public float searchDuration;
	public float shellSpeed;
    public Vector3 offset;
    public DetectObjectsInView detector;
	public ShootShell shooter;
	public ShootBullet muzzle;
	public Slider speedBoostSlider;
	public PotentialFunction potentialValues;

    [HideInInspector]
//    public Transform chaseTarget;
	public Vector3 chaseTarget;
    [HideInInspector]
    public NavMeshAgent navMesgAgent;
    [HideInInspector]
    public List<Transform> effectiveWaypoints;
//	[HideInInspector]
//	public Rigidbody body;

	public float lowHealthThreshhold = 25;

    //Constants
    public const string PATROL_STATE = "PATROL";
    public const string CHASE_STATE = "CHASE";
    public const string ALERT_STATE = "ALERT";
    public const string ATTACK_STATE = "ATTACK";
	public const string GET_AMMENITY_STATE = "GET_AMMENITY";
	public const string EVADE_STATE = "EVADE";

    // Dictionary of all the posiblle states
    private Dictionary<string, EnemyState> states;
	private EnemyState currentState;
	private float boostTime;
	private bool boosted;
	private bool boosterEngaged = false;
	private GameObject currentAmmenity;
	private GameObject player;
	private bool turnTowardsPlayer;
	private Rigidbody body;
	private Vector3 playerPosition;
//	private int health;

    void Awake() {
        //Build the dictionary and set the current state
        states = new Dictionary<string, EnemyState>();
        EnemyState state = new PatrolState(this, PATROL_STATE);
        states.Add(PATROL_STATE, state);

        currentState = state;

        state = new AlertState(this, ALERT_STATE);
        states.Add(ALERT_STATE, state);
        state = new ChaseState(this, CHASE_STATE);
        states.Add(CHASE_STATE, state);
        state = new AttackState(this, ATTACK_STATE);
        states.Add(ATTACK_STATE, state);
		state = new GetAmmenityState(this, GET_AMMENITY_STATE);
		states.Add(GET_AMMENITY_STATE, state);
		state = new EvadeState(this, EVADE_STATE);
		states.Add(EVADE_STATE, state);

        navMesgAgent = GetComponent<NavMeshAgent>();
        Debug.Log("Number of states:::"+states.Count);
        SetEffectiveWaypoints();
//		health = GetComponent<HealthManager> ().GetHealth();
		body = GetComponent<Rigidbody>();
    }

	public void AddBoost(float boost){
		boostTime = boost;
		if(!boosted)
			navMesgAgent.speed += 10;
		boosted = true;
		boosterEngaged = true;
	}
    
    // Update is called once per frame
    void Update () {
		if (boosterEngaged) {
			if (boosted) {
				if (boostTime == 0) {
					navMesgAgent.speed -= 10;
					boosted = false;
				} else {
					boostTime -= Time.deltaTime;
				}
			}else {
				navMesgAgent.speed += 10;
				boosted = true;
			}
		}else {
			if(boosted){
				navMesgAgent.speed -= 10;
				boosted = false;
			}
		}
		speedBoostSlider.value = boostTime;
        currentState.PerformAction();
	}

	void FixedUpdate(){
		currentState.PerformPhysicsAction();
//		Debug.Log("CurrentState state:::"+ currentState);
//		Debug.Log("Turn value:::"+ turnTowardsPlayer);
//		if(turnTowardsPlayer){
//			body.MoveRotation (Quaternion.Slerp(body.rotation, Quaternion.LookRotation(playerPosition), 2));
//		}
	}

    void OnTriggerEnter(Collider collider) {
        //On entering the collider, change the state to Alert
        currentState.MoveToState(states[ALERT_STATE]);
    }

    //void OnTriggerExit() {
    //    //On entering the collider, change the state to Alert
    //    //Debug.Log("In zone3");
    //    //Debug.Log("Current State:::" + currentState.GetName());
    //    currentState.MoveToState(states[PATROL_STATE]);
    //}

    /*
    Set the current state to the given state
*/
    public void SetCurrentState(IObjectState state)
    {
        currentState = state as EnemyState;
    }

    /*
        Get the current state
    */
    public EnemyState GetCurrentState()
    {
        return currentState;
    }

    /*
        Get the patrol state
    */
    public EnemyState GetPatrolState()
    {
        return states[PATROL_STATE];
    }

    /*
        Get the alert state
    */
    public EnemyState GetAlertState()
    {
        return states[ALERT_STATE];
    }

    /*
        Get the chase state
    */
    public EnemyState GetChaseState()
    {
        return states[CHASE_STATE];
    }

    /*
        Get the attack state
    */
    public EnemyState GetAttackState()
    {
        return states[ATTACK_STATE];
    }

	/*
        Get the fetch ammenity state
    */
	public EnemyState GetAmmenityState()
	{
		return states[GET_AMMENITY_STATE];
	}

	/*
        Get the fetch ammenity state
    */
	public EnemyState GetEvadeState()
	{
		return states[EVADE_STATE];
	}

	/*
        Set the target ammenity object
    */
	public void SetAmmenity(GameObject obj)
	{
		currentAmmenity = obj;
	}

	/*
        Get the target ammenity object
    */
	public GameObject GetAmmenity()
	{
		return currentAmmenity;
	}

	/*
        Get whether the speed is boosted or not
    */
	public bool GetBoosted()
	{
		return boosted;
	}

	/*
        Get whether the speed can be boosted or not
    */
	public bool IsBoostPossible()
	{
		return boostTime > 0 ? true : false;
	}

	/*
        Engage the speed Boosters
    */
	public void EngageBooster(){
		boosterEngaged = true;
	}

	/*
        Disengage the speed Boosters
    */
	public void DisEngageBooster(){
		boosterEngaged = false;
	}

    /*
        Reset the waypoints for the enemy
        */
    public void SetEffectiveWaypoints() {
        effectiveWaypoints = new List<Transform>(this.waypoints.Length);
        foreach (Transform t in waypoints) {
            effectiveWaypoints.Add(t);
        }
    }

	public Vector3 GetPlayerPosition(){
		if (!player)
			player = GameObject.FindGameObjectWithTag ("Player");
		return player.transform.position;
	}

	public void TurnTowardsPlayer(bool value,Vector3 pos){
		turnTowardsPlayer = value;
		playerPosition = pos;
	}
}
