﻿using UnityEngine;
using System.Collections;

public class EvadeState : EnemyState {

	private Vector3 evadeDirection;
	private bool inRange;

	//Constructor
	public EvadeState(EnemyStateManager stateManger,string name)
	{
		enemyStateManager = stateManger;
		stateName = name;
	}

	public override void PerformAction()
	{
		FindEvadeDirection ();
		CheckRange ();
		Boost ();
	}

	private void FindEvadeDirection(){
		evadeDirection = enemyStateManager.transform.position - enemyStateManager.GetPlayerPosition ();
//		enemyStateManager.navMesgAgent.destination = evadeDirection;
//		enemyStateManager.navMesgAgent.Resume ();
		enemyStateManager.navMesgAgent.Stop ();
		Rigidbody body = enemyStateManager.GetComponent<Rigidbody> ();
		body.rotation = Quaternion.Slerp (body.rotation, Quaternion.LookRotation(evadeDirection), 2f * Time.deltaTime);
		Vector3 targetPosition = evadeDirection.normalized * 10 * Time.deltaTime;
		enemyStateManager.transform.position += targetPosition;
	}

	private void CheckRange(){
		float d = evadeDirection.magnitude;
		if (d > 7) {
			inRange = false;
		} else {
			inRange = true;
		}
	}
	private void Boost(){
		if (inRange) {
			if (enemyStateManager.IsBoostPossible ()) {
				enemyStateManager.EngageBooster ();
			}
		} else {
			if(enemyStateManager.GetBoosted()){
				enemyStateManager.DisEngageBooster ();
				
			}
		}
	}
}
