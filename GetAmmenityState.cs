﻿using UnityEngine;
using System.Collections;

public class GetAmmenityState : EnemyState {

	private GameObject ammenity;
	private Vector3 pos;

	//Constructor
	public GetAmmenityState(EnemyStateManager stateManger, string name) {
		enemyStateManager = stateManger;
		stateName = name;
	}

	public override void PerformAction() {
		CheckAmmenityExist ();
		Move();
	}

	public override void OnStateEnter ()
	{
//		Debug.Log ("In ammenity");
		ammenity = enemyStateManager.GetAmmenity ();
		FindTargetLocation ();
	}

	//Check if any ammenities are currently up for grab or not
	private void FindTargetLocation(){
		RaycastHit hit;
		//Check if this object is falling or already on ground
		if (ammenity.GetComponent<Rigidbody> ().velocity == Vector3.zero) {
			pos = ammenity.transform.position;
		} else {
			if (Physics.Raycast (ammenity.transform.position, -ammenity.transform.up, out hit, 100, 8)) {
				pos = hit.transform.position;
			}
		}
	}

	// Move towards the designated target
	private void Move()
	{
		enemyStateManager.navMesgAgent.destination = pos;
		enemyStateManager.navMesgAgent.Resume();
		if(CheckBoostPossible()){
			enemyStateManager.EngageBooster ();
		}
	}

	// Move towards the designated target
	private void CheckAmmenityExist()
	{
		if(!ammenity){
			MoveToState (enemyStateManager.GetPatrolState());
			enemyStateManager.DisEngageBooster ();
		}
	}

	// Check whether the boost is possible or not
	private bool CheckBoostPossible()
	{
		return enemyStateManager.IsBoostPossible ();
	}
}
