﻿using UnityEngine;
using System.Collections;
using System;

public abstract class EnemyState : IObjectState {

    // Name of the state
    protected string stateName;

    // The state Manager instance
    protected EnemyStateManager enemyStateManager;

    /*
        Change the current state 
        to the given state
    */
    public virtual void MoveToState(IObjectState newState)
    {
        if (enemyStateManager.GetCurrentState().Equals(newState))
            return;

        enemyStateManager.SetCurrentState(newState as EnemyState);
        newState.OnStateEnter();
    }

    /*
        the actions which need to be done when the
        state changes to the current state
    */
    public virtual void OnStateEnter()
    {
        //Debug.Log("Entered the state::::" + stateName);
    }

    /*
        The actions which need to be done when the
        in the current state
    */
    public virtual void PerformAction()
    {
    }

	/*
        The physics actions which need to be done when the
        in the current state
    */
	public virtual void PerformPhysicsAction()
	{
	}

    public string GetName() {
        return stateName;
    }
}
