﻿using UnityEngine;
using System.Collections;

public class AlertState : EnemyState
{
    private float searchTimer;
    private int turningDirection;

    //Constructor
    public AlertState(EnemyStateManager stateManger,string name)
    {
        enemyStateManager = stateManger;
        stateName = name;
    }

    public override void PerformAction()
    {
        Look();
        Search();
    }

    /*
        the actions which need to be done when the
        state changes to the current state
    */
    public override void OnStateEnter()
    {
        //Randomly assign a tuning direction to the enemy
        turningDirection = Random.Range(0f, 1f) < 0.5f ? 1 : -1;
    }

    //Check for the existence of target object in the view
    private void Look()
    {
        RaycastHit hit = enemyStateManager.detector.Shootrays();

        if (hit.transform != null)
        {
            MoveToState(enemyStateManager.GetChaseState());
//            enemyStateManager.chaseTarget = hit.transform;
			enemyStateManager.chaseTarget = hit.transform.position;
            searchTimer = 0;
        }
    }

    private void Search() {
        enemyStateManager.navMesgAgent.Stop();
        
        enemyStateManager.transform.Rotate(0,turningDirection * enemyStateManager.turningSpeed *Time.deltaTime, 0);
        searchTimer += Time.deltaTime;
        if (searchTimer > enemyStateManager.searchDuration) {
            searchTimer = 0;
            MoveToState(enemyStateManager.GetPatrolState());
        }
    }
}
