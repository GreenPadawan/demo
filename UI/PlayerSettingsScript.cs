﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerSettingsScript : MonoBehaviour {

	public InputField playerName;
	public InputField playerColour;

	void Awake () {
		GetPlayerDetails ();
	}

	void OnEnable () {
		GetPlayerDetails ();
	}

	private void GetPlayerDetails(){
		if(playerName.text.Equals("")){
			playerName.text = !PlayerPrefs.GetString ("PLAYER_NAME").Equals ("") ? PlayerPrefs.GetString ("PLAYER_NAME") : "";	
		}
		if (playerColour.text.Equals ("")) {
			playerColour.text = !PlayerPrefs.GetString ("PLAYER_COLOUR").Equals ("") ? PlayerPrefs.GetString ("PLAYER_COLOUR") : "";
		}
	}
}
