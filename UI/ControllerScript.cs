﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ControllerScript : MonoBehaviour {

    [SerializeField]
    Image joystick;

    [SerializeField]
    Button buttonA;

    [SerializeField]
    Button buttonB;

    [SerializeField]
    Button buttonC;

    [SerializeField]
    Button buttonD;

    [SerializeField]
    GameObject player;

    public int joystickScale;

    private SimpleTouchPad joystickTouchpad;
	private ShootShell launcher;
	private ShootBullet muzzle;
	private Rigidbody body;
	private Movement mover;
	private GameObject rearViewCamera;
	private float timer;
	private float shellTimer;
	private float fireRate = 1.5f;
	private float bulletfireRate = 0.2f;
	private bool shootBullet;
    //private SimpleTouchButton button_A;
    //private SimpleTouchButton button_B;
    //private SimpleTouchButton button_C;
    //private SimpleTouchButton button_D;

    void Awake() {
        joystickTouchpad = joystick.GetComponent<SimpleTouchPad>();
		launcher = player.GetComponentInChildren<ShootShell> ();
		muzzle = player.GetComponentInChildren<ShootBullet> ();
		body = player.GetComponent<Rigidbody> ();
		mover = player.GetComponent<Movement> ();
		rearViewCamera = player.GetComponentInChildren<Camera> ().gameObject;

        //button_A = buttonA.GetComponent<SimpleTouchButton>();
        //button_B = buttonB.GetComponent<SimpleTouchButton>();
        //button_C = buttonC.GetComponent<SimpleTouchButton>();
        //button_D = buttonD.GetComponent<SimpleTouchButton>();

        //Set the default value
        if (joystickScale == 0) {
            joystickScale = 1;
        }
    }
    

    //This method is used to perform operation whenever the joystick control
    // is used for providing input
    public void JoystickControl()
    {
		Vector2 movement = getDirection();
		Vector3 velocity = player.transform.forward * movement.y;
		float rotation = Mathf.Abs(movement.x);
		if (movement.x > 0) {
			body.MoveRotation (Quaternion.LookRotation (
				Vector3.RotateTowards (player.transform.forward, -player.transform.right, mover.rotationSpeed * Mathf.Deg2Rad, rotation * Mathf.Deg2Rad)));	
		} else if(movement.x < 0){
			body.MoveRotation( Quaternion.LookRotation(
				Vector3.RotateTowards(player.transform.forward, player.transform.right, mover.rotationSpeed * Mathf.Deg2Rad, rotation * Mathf.Deg2Rad)));
		}
		body.velocity = velocity * mover.speed;

    }

    // Overloaded method to move the game object with the calculated velocity
    public void JoystickControl(GameObject go)
    {
        Vector2 movement = getDirection();
        Rigidbody body = go.GetComponent<Rigidbody>();
		Vector3 velocity = player.transform.forward * movement.y;
		body.velocity = velocity * go.GetComponent<Movement>().speed;
		float rotation = Mathf.Abs(movement.x);
		if (movement.x > 0) {
			body.MoveRotation (Quaternion.LookRotation (
				Vector3.RotateTowards (player.transform.forward, transform.right, mover.rotationSpeed * Mathf.Deg2Rad, rotation * Mathf.Deg2Rad)));	
		} else if(movement.x < 0){
			body.MoveRotation( Quaternion.LookRotation(
				Vector3.RotateTowards(player.transform.forward, -transform.right, mover.rotationSpeed * Mathf.Deg2Rad, rotation * Mathf.Deg2Rad)));
		}
    }


	//******************A Button Handling******************
	public void ButtonAClicked(GameObject go) {
		if (shellTimer > fireRate) {
			go.GetComponentInChildren<ShootShell> ().Shoot (go.GetComponentInChildren<ShootShell> ().shellSpeed);
			shellTimer = 0;
		}
	}
	//Fire the shell
	public void ButtonAClicked() {
		if(shellTimer > fireRate){
			launcher.Shoot (launcher.shellSpeed);
			shellTimer = 0;
		}
	}


	//******************B Button Handling******************
	public void ButtonBClicked(GameObject go)
	{
		go.GetComponent<Movement> ().SetStop(true);
	}

	//Stop the movement
	public void ButtonBClicked()
	{
		mover.SetStop (true);
	}


	//******************C Button Handling******************
	public void ButtonCClicked(GameObject go)
	{
//		GameObject cam = go.GetComponentInChildren<Camera> ().gameObject;
//		if (!cam)
//			return;
//		
//		cam.SetActive (!cam.activeSelf);
		go.GetComponentInChildren<ShootBullet>().Shoot();
	}

	//Toggle the rear view camera
	public void ButtonCClicked()
	{
//		rearViewCamera.SetActive (!rearViewCamera.activeSelf);
		muzzle.Shoot ();
	}

	//Toggle the rear view camera
	public void ButtonCPressed()
	{
		shootBullet = true;
	}
	//Toggle the rear view camera
	public void ButtonCReleased()
	{
		shootBullet = false;
	}


	//******************D Button Handling******************
	public void ButtonDClicked(GameObject go)
	{
		go.GetComponent<Movement> ().ToggleBoosterEngaged();
	}

	//Engage/Disengage the speed booster
	public void ButtonDClicked()
	{
		mover.ToggleBoosterEngaged ();
	}

    //Retrieve the direction of movement
    private Vector2 getDirection() {
        float moveHorizontal, moveVertical;
        Vector2 direction = joystickTouchpad.getDirection();
        moveHorizontal = direction.x * joystickScale;
        moveVertical = direction.y * joystickScale;
        return new Vector2(moveHorizontal,moveVertical);
    }

    void Update () {
		timer += Time.deltaTime;
		shellTimer += Time.deltaTime;
		if (timer > bulletfireRate) {
			timer = 0;
			if (shootBullet) {
				muzzle.Shoot ();
			}
		}
	}


	//******************Up Button Handling******************
	//Move up
	public void ButtonUpPressed() {
		mover.SetUp (true);
	}

	//Move up
	public void ButtonUpReleased() {
		mover.SetUp (false);
	}


	//******************Down Button Handling******************
	//Move down
	public void ButtonDownPressed()
	{
		mover.SetDown (true);
	}

	//Move down
	public void ButtonDownReleased()
	{
		mover.SetDown (false);
	}


	//******************Right Button Handling******************
	//Rotate Right
	public void ButtonRightPressed()
	{
		mover.SetRight (true);
	}

	//Rotate left
	public void ButtonRightReleased()
	{
		mover.SetRight (false);
	}


	//******************Left Button Handling******************
	//Rotate left
	public void ButtonLeftPressed()
	{
		mover.SetLeft (true);
	}

	//Rotate left
	public void ButtonLeftReleased()
	{
		mover.SetLeft (false);
	}

}
