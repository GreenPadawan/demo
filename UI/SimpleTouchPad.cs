﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SimpleTouchPad : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IDragHandler {

	public float smoothing;

	private Vector2 origin;
	private Vector2 direction;
	private Vector2 smoothDirection;
	private bool touched;
	private int pointerID;

	void Awake(){
		direction = Vector2.zero;
		touched = false;
	}

	public void OnPointerDown(PointerEventData data){
        //Set the start point
        if (!touched)
        {
            touched = true;
            pointerID = data.pointerId;
            origin = data.position;
        }
    }

	public void OnPointerUp(PointerEventData data){
		// Reset everything
		if (data.pointerId == pointerID) {
			direction = Vector2.zero;
			touched = false;
		}
	}

	public void OnDrag(PointerEventData data){
        //Compare the difference between the start and the current point
        if (data.pointerId == pointerID){
            Vector2 currentPosition = data.position;
			Vector2 dir = currentPosition - origin;
			direction = dir.normalized;
            //Reset the origin to the current position, so that
            // the new dirextion is correctly calculated if the
            // user changes the direction of drag without lifting
            // his finger.
            origin = currentPosition;
		}
	}
    
	public Vector2 getDirection(){
		smoothDirection = Vector2.MoveTowards (smoothDirection, direction, smoothing);
		return smoothDirection;
	}
}
