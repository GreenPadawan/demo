﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SimpleTouchButton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler {

	private bool touched;
	private int pointerID;
	private bool canFire;

	void Awake(){
		touched = false;
	}

	public void OnPointerDown(PointerEventData data){
		//Set the start point
		if (!touched) {
			touched = true;
			pointerID = data.pointerId;
			canFire = true;
		}
	}

	public void OnPointerUp(PointerEventData data){
		// Reset everything
		if (data.pointerId == pointerID) {
			touched = false;
			canFire = false;
		}
	}

	public bool CanFire(){
		return canFire;
	}
}
