﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Tab {
    public Button button;
    public GameObject tabPanel;
}

public class TabstripManager : MonoBehaviour {

    [SerializeField]
    GameObject displayPanel;
    [SerializeField]
    Tab[] tabs;

    private Dictionary<int,Tab> buttonTabs = new Dictionary<int, Tab>();
    private int lastOpenTab;

    void Awake() {
        //Build the dictionary of the buttons and their corresponding tabs.
        foreach (Tab tab in tabs) {
            int id = tab.button.GetComponent<TabstripButton>().id;
            buttonTabs.Add(id, tab);
        }
    }

    void Start () {
        //By default, display the first tab
        if (buttonTabs.Count >= 1) {
            SelectButton(1);
        }
    }
	
    /*
      Display the tab for the button which is clicked
    */
    public void DisplayTab(GameObject go) {
        TabstripButton tsButton = go.GetComponent<TabstripButton>();
        if (tsButton == null) {
            return;
        }

        int id = tsButton.id;
        SelectButton(id);
    }

    /*
        Select a button and display its corresponding panel
    */
    private void SelectButton(int id) {
        Button b = GetTab(id).button;
        b.Select();
        GameObject panelToDisplay = GetTab(id).tabPanel;
        //Disable the previously opened tab
        GetTab(lastOpenTab).tabPanel.SetActive(false);
        panelToDisplay.SetActive(true);

        lastOpenTab = id;
    }

    /*
        Get the tab for the button which is clicked
    */
    private Tab GetTab(int id) {
        if (buttonTabs.ContainsKey(id))
            return buttonTabs[id];
        else
            return buttonTabs[1];
    }
}
