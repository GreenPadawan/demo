﻿using UnityEngine;
using System.Collections;

public interface IObjectState {
    
    /*
        Change the current state 
        to the given state
    */
    void MoveToState(IObjectState newState);

    /*
        The actions which need to be done when the
        state changes to the current state
    */
    void OnStateEnter();

    /*
        The actions which need to be done when the
        in the current state
    */
    void PerformAction();

}
