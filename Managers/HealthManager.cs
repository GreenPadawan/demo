﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
using System.Collections;

public class HealthManager : MonoBehaviour {

    public Slider healthSlider;
	public Slider shieldSlider;
	private bool shieldActive;
	private float shieldTimer;
    private int health;

    public int AffectDamage(int value) {
		if (shieldActive)
			return health;
		health -= value;
        health = health <= 0 ? 0 : health;
        healthSlider.value = health;
        return health;
    }

	public int RegenerateHealth(int value){
		health += value;
		health = health>100? 100 : health;
		healthSlider.value = health;
		return health;
	}
	    
	void Awake () {
        health = 100;
        if (healthSlider == null) {
            Debug.LogError("Assign the slider component");
        }
	}

    void Update() {
        if (health <= 0) {
            Debug.Log(this.gameObject.tag + " killed");
            health = 100;
            healthSlider.value = health;
			Destroy (gameObject);
//			GameObject.FindGameObjectWithTag ("GameManager").GetComponent<TimeManager>().ManipulateTime(0,1);
			GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager>().roundOver = true;
        }

		if(shieldActive){
			shieldTimer -= Time.deltaTime;			
			if (shieldTimer <= 0) {
				shieldTimer = 0;
				shieldActive = false;
				Behaviour halo =(Behaviour)gameObject.GetComponent ("Halo");
				halo.enabled = false;
			} else {
				shieldSlider.value = shieldTimer;
			}
		}
    }

	public int GetHealth(){
		return health > 100 ? 100 : health;
	}

	public void ActivateShield(float value){
		shieldActive = true;
		shieldTimer += value;
		shieldTimer = shieldTimer > 100 ? 100 : shieldTimer;
		Behaviour halo = (Behaviour)gameObject.GetComponent ("Halo");
		halo.enabled = true;
	}
}
