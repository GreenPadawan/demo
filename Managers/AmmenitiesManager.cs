﻿using UnityEngine;
using System.Collections;

public class AmmenitiesManager : MonoBehaviour {

	public GameObject[] prefabs;
	public float xMax, xMin, zMax, zMin;
	public float delay;
	public float originHeight;

	// Use this for initialization
	void Start () {
		StartCoroutine (GenerateAmmenities());
	}

	IEnumerator GenerateAmmenities(){
		while(true){
//		GameObject obj = GameObject.Instantiate (prefabs[Random.Range(0, prefabs.Length)]) as GameObject;
			GameObject prefab = prefabs[Random.Range(0, prefabs.Length)];
			GameObject obj = GameObjectUtil.Instantiate(prefab, prefab.transform.position);
			obj.transform.position = transform.position;
			obj.transform.rotation = transform.rotation;
			obj.transform.position = new Vector3 (Random.Range (xMin, xMax), originHeight, Random.Range (zMin, zMax));//obj.transform.position.y
			obj.SetActive (true);
			yield return new WaitForSeconds (delay);
		}

	}
}
