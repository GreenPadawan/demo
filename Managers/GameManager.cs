﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour {

	public int gameDifficulty;
	public GameObject mobileController;
	public GameObject player;
	public bool	roundOver;
	public Animator sliderPanel;

	private GameObject rearViewCamera;
	private ModalPanelManager modalManager;
	private TimeManager timeManager;
	private bool isMobile;
	private bool fadedIn;


	void Awake(){
//		DontDestroyOnLoad (this);
		if (Application.isMobilePlatform) {
			isMobile = true;
			mobileController.SetActive (true);
		} else {
			isMobile = false;
			mobileController.SetActive (false);
		}
		rearViewCamera = player.GetComponentInChildren<Camera>().gameObject;
		rearViewCamera.SetActive (false);
		modalManager = ModalPanelManager.GetInstance ();
		timeManager = GetComponent<TimeManager> ();
	}

	// Use this for initialization
	void Start () {
		
	}

	void OnEnable(){
		if (Application.isMobilePlatform) {
			isMobile = true;
			mobileController.SetActive (true);
		} else {
			isMobile = false;
			mobileController.SetActive (false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)){
//			mobileController.SetActive (false);
			if (!fadedIn) {
//				fadedIn = true;
//				sliderPanel.SetBool ("FadeIn", true);
//				sliderPanel.SetBool ("FadeOut", false);
//				timeManager.ManipulateTime (0, 1);
			} else {
//				fadedIn = false;
//				sliderPanel.SetBool ("FadeOut", true);
//				sliderPanel.SetBool ("FadeIn", false);
//				timeManager.ManipulateTime (1, 1);
			}

//			UnityAction commonAction = new UnityAction (CancelExitFromGame);
//			modalManager.CreateExitGameWindow (commonAction,commonAction);
		}
		if(roundOver){
			timeManager.ManipulateTime (0, 1);
			roundOver = false;
			modalManager.CreateMessageBox("Round Over", "Round is over, press Ok to restart!!", new UnityAction(RestartGame));
		}
		if(Input.GetKeyDown(KeyCode.C)){
			captureScreenShot ();
		}
	}

	void OnApplicationPause(bool pauseStatus){
		if (!pauseStatus)
			return;
		modalManager.CreateMessageBox ("Game Paused", "Press Ok to resume the game.", new UnityAction(ResumeFromPause));
		timeManager.ManipulateTime (0, 1);
	}

	void ResumeFromPause(){
		timeManager.ManipulateTime (1,3);
	}

	void CancelExitFromGame(){
		if(isMobile)
			mobileController.SetActive (true);
		timeManager.ManipulateTime (1, 1);
	}

	void RestartGame(){
		SceneManager.LoadScene ("Scene_AI_Demo");
		SceneManager.UnloadScene ("Scene_AI_Demo");
//		timeManager.ManipulateTime (1,3);
	}

	public bool IsMobile(){
		return isMobile;
	}

	private void captureScreenShot(){
		Application.CaptureScreenshot ("C:\\Users\\chirayu\\Desktop\\Screen.png");
	}

	public void ExitGame(){
		Application.Quit();
//		modalManager.CreateExitGameWindow (() => {Application.Quit();});
	}
}
