﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuManager : MonoBehaviour {

	public Animator	sliderPanel;
	public GameObject mobileController;
	public TimeManager timeManager;
	public GameManager gameManager;
	public InputField playerColour;
	public InputField playerName;
//	public Animator tabstripPanel;
	public GameObject tabstripPanel;

	private bool fadedIn;
	private bool TabStripFadedIn;

	void Awake(){
		TabStripFadedIn = false;
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)){
			mobileController.SetActive (false);
			if (!fadedIn) {
				FadeIn ();
			} else {
				FadeOut ();
			}
		}
	}

	private void FadeOut(){
		fadedIn = false;
		sliderPanel.SetBool ("FadeOut", true);
		sliderPanel.SetBool ("FadeIn", false);
		timeManager.ManipulateTime (1,3);
	}

	private void FadeIn(){
		fadedIn = true;
		sliderPanel.SetBool ("FadeIn", true);
		sliderPanel.SetBool ("FadeOut", false);
		timeManager.ManipulateTime (0,3);
	}

	public void ResumeGame(){
		FadeOut ();
	}

	public void ExitGame(){
		gameManager.ExitGame ();
	}

	public void SavePlayerSettings(){
		PlayerPrefs.SetString ("PLAYER_NAME", playerName.text);
		PlayerPrefs.SetString ("PLAYER_COLOUR", playerColour.text);
	}

	public void ToggleSettingsPanel(){
//		if (!TabStripFadedIn) {
//			TabStripFadeIn ();
//		} else {
//			TabStripFadeOut ();
//		}
		tabstripPanel.SetActive(!tabstripPanel.activeSelf);
	}

	private void TabStripFadeOut(){
//		TabStripFadedIn = false;
//		tabstripPanel.SetBool ("FadeOut", true);
//		tabstripPanel.SetBool ("FadeIn", false);
	}

	private void TabStripFadeIn(){
//		TabStripFadedIn = true;
//		tabstripPanel.SetBool ("FadeIn", true);
//		tabstripPanel.SetBool ("FadeOut", false);
	}
}
