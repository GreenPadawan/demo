﻿using UnityEngine;
using System.Collections;

public class ColorChanger : MonoBehaviour {
    public Color color;

	// Use this for initialization
	void Start () {
		GetComponentInChildren<MeshRenderer>().material.color = color;
	}
}
