﻿using UnityEngine;
using System.Collections;

public class DestroyOnTime : MonoBehaviour {

	public float lifetime;
	private float timer;
	// Use this for initialization
	void Start () {
		timer = lifetime;
//		Destroy (gameObject, lifetime);
	}
	void OnEnable(){
		timer = lifetime;
	}

	void Update(){
		timer -= Time.deltaTime;
		if(timer <= 0){
			GameObjectUtil.Destroy (gameObject);
		}
	}
}
