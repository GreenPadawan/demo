﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Movement : MonoBehaviour {
    public float speed;
    public float rotationSpeed;
    public GameObject floorViewer;
    public float onGroundThreshhold;
	public GameObject rearViewCamera;
	public Slider speedBoostSlider;

	private bool up;
	private bool down;
	private bool right;
	private bool left;
	private bool stop;

    private Rigidbody body;
	private float boostTime;
	private bool boosted;
	private bool boosterEngaged = false;
	private float maxRotationSpeed;
//    int floorMask;                      // A layer mask so that a ray can be cast just at gameobjects on the floor layer.
//    float camRayLength = 100f;

    void Start () {
//        floorMask = LayerMask.GetMask("Floor");
        body = GetComponent<Rigidbody>();
		boostTime = 0;
	}

	public void AddBoost(float boost){
		boostTime += boost;
		boostTime = boostTime >= 50 ? 50 : boostTime;
		if(!boosted) 
			speed += 10;
		boosted = true;
		boosterEngaged = true;
	}

    private bool CheckStandingOnFloor() {
        RaycastHit hit;
        bool isStandingOnFloor;
        Ray ray = new Ray();
        ray.origin = floorViewer.transform.position;
        ray.direction = floorViewer.transform.forward;
        //Shoot aray from the floor viewer to check the distance from the ground
        if (Physics.Raycast(ray, out hit, onGroundThreshhold))
        {
            if (hit.collider.CompareTag("Floor"))
            {
                isStandingOnFloor = true;
            }
            else {
                isStandingOnFloor = false;
            }
        }
        else {
            isStandingOnFloor = false;
        }

        return isStandingOnFloor;
    }

	//All physics manipulation should be done in this method
	void FixedUpdate(){
		if (up) {
			body.velocity = transform.forward * speed;
		}
		if (down)
		{
			body.velocity = -transform.forward * speed;
		}

		if (left)
		{
			body.MoveRotation(Quaternion.LookRotation(
					Vector3.RotateTowards(transform.forward, -transform.right, rotationSpeed * Mathf.Deg2Rad, 0.5f * Mathf.Deg2Rad)));
		}
		if (right)
		{
			body.MoveRotation (Quaternion.LookRotation (
					Vector3.RotateTowards (transform.forward, transform.right, rotationSpeed * Mathf.Deg2Rad, 0.5f * Mathf.Deg2Rad)));
		}

		if(stop){
			body.velocity = Vector3.zero;
			stop = false;
		}
	}
	
	void Update () {
//        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
//
//        // Create a RaycastHit variable to store information about what was hit by the ray.
//        RaycastHit floorHit;
//
        // Perform the raycast and if it hits something on the floor layer...
//        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
//        {
//            // Create a vector from the player to the point on the floor the raycast from the mouse hit.
//            Vector3 playerToMouse = floorHit.point - transform.position;
//
//            // Ensure the vector is entirely along the floor plane.
//            playerToMouse.y = 0f;
//
//            // Create a quaternion (rotation) based on looking down the vector from the player to the mouse.
//            Quaternion newRotatation = Quaternion.LookRotation(playerToMouse);
//
//            // Set the player's rotation to this new rotation.
//            body.MoveRotation(newRotatation);
//        }
		if(Input.GetKeyDown(KeyCode.LeftShift)){
			boosterEngaged = !boosterEngaged;
		}

		if (boosterEngaged) {
			if (boosted) {
				if (boostTime <= 0) {
					speed -= 10;
					boosted = false;
					boosterEngaged = false;
				} else {
					boostTime -= Time.deltaTime;
				}
			} else {
				speed += 10;
				boosted = true;
			}
		} else {
			if(boosted){
				speed -= 10;
				boosted = false;
			}
		}
		speedBoostSlider.value = boostTime;


        if (Input.GetKey(KeyCode.W)) {
			up = true;
        }
        if (Input.GetKey(KeyCode.S))
        {
			down = true;
        }
		if (Input.GetKey(KeyCode.A))
        {
            if (CheckStandingOnFloor())
            {
				left = true;
            }
        }
        if (Input.GetKey(KeyCode.D))
        {
			if (CheckStandingOnFloor ()) {
				right = true;
			}
        }

		if (Input.GetKeyUp(KeyCode.W)) {
			up = false;
		}
		if (Input.GetKeyUp(KeyCode.S))
		{
			down = false;
		}

		if (Input.GetKeyUp(KeyCode.A))
		{
			if (CheckStandingOnFloor())
			{
				left = false;
			}
		}
		if (Input.GetKeyUp(KeyCode.D))
		{
			if (CheckStandingOnFloor ()) {
				right = false;
			}
		}

		if(Input.GetKeyDown(KeyCode.B)){
			rearViewCamera.SetActive (!rearViewCamera.activeSelf);
		}

		if(Input.GetKeyDown(KeyCode.Space)){
			stop = true;
		}
    }


	//Check whether the speed booster have been engaged or not
	public bool GetBoosted(){
		return boosted;
	}

	public void ToggleBoosterEngaged(){
		boosterEngaged = !boosterEngaged;
	}

	public void SetUp(bool value){
		up = value;
	}

	public bool GetUp(){
		return up;
	}

	public void SetDown(bool value){
		down = value;
	}

	public bool GetDown(){
		return down;
	}
	public void SetLeft(bool value){
		left = value;
	}

	public bool GetLeft(){
		return left;
	}
	public void SetRight(bool value){
		right = value;
	}

	public bool GetRight(){
		return right;
	}

	public void SetStop(bool value){
		stop = value;
	}

	public bool GetStop(){
		return stop;
	}
}
