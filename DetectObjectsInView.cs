﻿using UnityEngine;
using System.Collections;

public class DetectObjectsInView : MonoBehaviour {

    public float rayRange;
    public LayerMask[] detectableComponents;
    public string[] detectableTags;

    private Camera cameraComponent;
    private RaycastHit hit;
    private float fov;
	private float height;

    //Initialize
    void Awake() {
        cameraComponent = GetComponent<Camera>();
        fov = cameraComponent.fieldOfView;
		height = cameraComponent.pixelHeight;
		Debug.Log ("Camera Height:::"+ height);
    }

	void Update () {
       //hit = Shootrays();
	}

    /*
    Shoot rays out of the camera in order to detect the presence of 
    objects
    */
    public RaycastHit Shootrays() {
        Ray leftRay = new Ray();
        Ray rightRay = new Ray();
//		Ray leftRay1 = new Ray();
//		Ray rightRay1 = new Ray();
        RaycastHit rayHit = new RaycastHit();
        Vector3 leftDirection, rightDirection;
//		bool hitRay =false;

//		for(int j = 0; j<height;j++){
//			Vector3 hVector = new Vector3 (0,j,0);
//			Vector3 originUp = transform.position + hVector;
//			Vector3 originDown = transform.position - hVector;

			leftRay.origin = transform.position;        
        	rightRay.origin = transform.position;
//			leftRay.origin = originUp;        
//			rightRay.origin = originUp;
//			leftRay1.origin = originDown;        
//			rightRay1.origin = originDown;

        //Shoot rays in the field of view of the camera to detect the objects
        for (int i = 0 ; i < fov/2 ; i++) {
            float angle = i * Mathf.Deg2Rad;
            leftDirection = transform.forward;
            rightDirection = transform.forward;
            //leftDirection = new Vector3(Mathf.Sin(angle), 0 , Mathf.Cos(angle));
            //rightDirection = new Vector3(-Mathf.Sin(angle), 0, Mathf.Cos(angle));
            leftDirection = Vector3.RotateTowards(leftDirection, -transform.right, angle, angle);
            rightDirection = Vector3.RotateTowards(rightDirection, transform.right, angle, angle);


            leftRay.direction = leftDirection;
            rightRay.direction = rightDirection;
//				leftRay1.direction = leftDirection;
//				rightRay1.direction = rightDirection;
            //if (Physics.Raycast(leftRay, out rayHit, rayRange, detectableComponents[0])) {
            if (Physics.Raycast(leftRay, out rayHit, rayRange) && rayHit.collider.tag == detectableTags[0]) {
                //if the ray hits any of the detectable components, then break from the loop
                // and return its destination
//					hitRay = true;
					break;
            }
            //if (Physics.Raycast(rightRay, out rayHit, rayRange, detectableComponents[0]))
            //{
            if (Physics.Raycast(rightRay, out rayHit, rayRange) && rayHit.collider.tag == detectableTags[0])
            {
                    //if the ray hits any of the detectable components, then break from the loop
                    // and return its destination
//					hitRay = true;
					break;
            }
//				if (Physics.Raycast(leftRay1, out rayHit, rayRange) && rayHit.collider.tag == detectableTags[0]) {
//					//if the ray hits any of the detectable components, then break from the loop
//					// and return its destination
//					hitRay = true;
//					break;
//				}
//				//if (Physics.Raycast(rightRay, out rayHit, rayRange, detectableComponents[0]))
//				//{
//				if (Physics.Raycast(leftRay1, out rayHit, rayRange) && rayHit.collider.tag == detectableTags[0])
//				{
//					//if the ray hits any of the detectable components, then break from the loop
//					// and return its destination
//					hitRay = true;
//					break;
//				}
            //Debug.Log(i + ">>" + "Left:::" + leftRay.origin + ">>>>>" + leftRay.direction);
            //Debug.Log(i + ">>" + "Right:::" + rightRay.origin + ">>>>>" + rightRay.direction);
            //Debug.DrawRay(leftRay.origin, leftRay.direction, Color.green, 5000, false);
            //Debug.DrawRay(rightRay.origin, rightRay.direction, Color.red, 5000, false);
			}
//			if(hitRay){
//				break;
//			}
//		}
        hit = rayHit;
        return rayHit;
    }

    /*
    Shoot rays out of the camera in the given direction order to detect the presence of 
    objects
    */
    public RaycastHit Shootrays(Vector3 direction)
    {
        Ray ray = new Ray();
        RaycastHit rayHit = new RaycastHit();
        ray.origin = transform.position;
        ray.direction = direction;
        Physics.Raycast(ray, out rayHit, rayRange);
        hit = rayHit;
        return rayHit;
    }


    /*
        Get the object struck
        */
    public RaycastHit GetHit() {
        return hit;
    }
}
