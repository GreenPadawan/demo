﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool : MonoBehaviour {
    public RecycleGameObject prefab;
    private List<RecycleGameObject> objectPool = new List<RecycleGameObject>();

    private RecycleGameObject CreateInstance(Vector3 pos) {
        var clone = GameObject.Instantiate(prefab);
        clone.transform.position = pos;
        clone.transform.parent = transform;
        objectPool.Add(clone);
        return clone;
    }

    public RecycleGameObject GetInstance(Vector3 pos) {
        RecycleGameObject instance = null;
        foreach (var go in objectPool) {
            if (go.gameObject.activeSelf != true) {
                instance = go;
                instance.transform.position = pos;
                break;
            }
        }
        if (instance == null)
        {
            instance = CreateInstance(pos);
        }
        
        instance.Restart();
        return instance;
    }
	private RecycleGameObject CreateInstanceUI(GameObject canvas)
	{
		var clone = GameObject.Instantiate(prefab);
		clone.gameObject.transform.SetParent(canvas.transform, false);
		//clone.transform.parent = transform;
		objectPool.Add(clone);
		return clone;
	}

	public RecycleGameObject GetInstanceUI(GameObject canvas)
	{
		RecycleGameObject instance = null;
		foreach (var go in objectPool)
		{
			if (go.gameObject.activeSelf != true)
			{
				instance = go;
				instance.transform.SetParent(canvas.transform, false);
				//instance.transform.position = pos;
				break;
			}
		}
		if (instance == null)
		{
			instance = CreateInstanceUI(canvas);
		}

		instance.Restart();
		return instance;
	}
}
