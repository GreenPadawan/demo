﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameObjectUtil
{
    private static Dictionary<RecycleGameObject, ObjectPool> pools = new Dictionary<RecycleGameObject, ObjectPool>();
     
    public static GameObject Instantiate(GameObject prefab, Vector3 pos)
    {
        GameObject instance = null;
        var recycleScript = prefab.GetComponent<RecycleGameObject>();
        if (recycleScript != null) {
            var pool = getPool(recycleScript);
            instance = pool.GetInstance(pos).gameObject;
        }
        else
        {
            instance = GameObject.Instantiate(prefab);
            instance.transform.position = pos;
        }
        return instance;
    }

	public static GameObject InstantiateUI(GameObject prefab, GameObject canvas)
	{
		GameObject instance = null;
		var recycleScript = prefab.GetComponent<RecycleGameObject>();
		if (recycleScript != null)
		{
			var pool = getPool(recycleScript);
			instance = pool.GetInstanceUI(canvas).gameObject;
		}
		else
		{
			instance = GameObject.Instantiate(prefab);
			//instance.transform.position = pos;
			instance.transform.SetParent(canvas.transform,false);
		}
		return instance;
	}

    public static void Destroy(GameObject prefab)
    {
        var recycleGameObject = prefab.GetComponent<RecycleGameObject>();
        if (recycleGameObject != null)
        {
            recycleGameObject.Shutdown();
        }
        else {
            GameObject.Destroy(prefab);
        }
    }

    private static ObjectPool getPool(RecycleGameObject reference) {
        ObjectPool pool = null;
        if (pools.ContainsKey(reference)) {
            pool = pools[reference];
        }
        else {
            var container = new GameObject(reference.gameObject.name + "ObjectPool");
            pool = container.AddComponent<ObjectPool>();
            pool.prefab = reference;
            pools.Add(reference,pool);
        }
        return pool;
    }
}