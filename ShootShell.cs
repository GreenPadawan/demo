﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShootShell : MonoBehaviour {

	public GameObject shell;
	public float shellSpeed;
	public float fireRate = 1.5f;
	public int shellsRemaining;
	public float projectileForce = 60f;
	public Text shellText;

	private GameObject obj;
	private float timer;
	private AudioSource source;
	private GameManager gameManager;
	private int maxShells = 10;
	private Light canonFlashLight;

	void Awake(){
		source = GetComponent<AudioSource> ();
		source.enabled = true;
		gameManager = GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager>();
		canonFlashLight = GetComponent<Light> ();
		shellText.text = shellText.text.Insert (shellText.text.IndexOf (":")+1,shellsRemaining.ToString());
	}

	void OnEnable(){

		shellText.text = shellText.text.Insert (shellText.text.IndexOf (":")+1,shellsRemaining.ToString());
	}

	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		float fire = Input.GetAxis("Fire2");
		if( fire !=0 && CompareTag("Player") && !gameManager.IsMobile()){
			if (timer > fireRate) {
				timer = 0;
				Shoot (shellSpeed);
			}
		}
		shellText.text = shellText.text.Remove (shellText.text.IndexOf (":")+1);
		shellText.text = shellText.text.Insert (shellText.text.IndexOf (":")+1,shellsRemaining.ToString());
	}

	public void Shoot(float speed){
		if (shellsRemaining == 0)
			return;
		obj = GameObjectUtil.Instantiate(shell,transform.position);
		obj.transform.rotation = Quaternion.identity;
		obj.transform.rotation = Quaternion.LookRotation(transform.forward,transform.up);
//		obj.transform.Rotate(90,0,0);
		obj.GetComponent<Rigidbody> ().velocity = transform.forward * speed;
//		obj.GetComponent<Rigidbody> ().AddForce(transform.forward * projectileForce);
//		obj.GetComponent<Rigidbody> ().ResetCenterOfMass ();
		obj.gameObject.SetActive (true);
		source.Play ();
		StartCoroutine (ShowCanonFlash());
		shellsRemaining--;
		StopCoroutine (ShowCanonFlash());
	}

	public int AddShells(int count){
		int c = count <= maxShells - shellsRemaining ? count : maxShells - shellsRemaining;
		shellsRemaining += c;
		return count - c;
	}

	IEnumerator ShowCanonFlash(){
		canonFlashLight.enabled = true;
		yield return new WaitForSeconds (0.05f);
		canonFlashLight.enabled = false;
	}
}
