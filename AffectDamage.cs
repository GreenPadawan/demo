﻿using UnityEngine;
using System.Collections;

public class AffectDamage : MonoBehaviour {

    private int selfDamage;
    private int damage;
    private HealthManager selfManager;

    void Awake() {
        damage = GetComponent<Damage>().damage;
        selfDamage = GetComponent<Damage>().selfDamage;
        selfManager = GetComponent<HealthManager>();
    }

    void OnTriggerEnter(Collider other) {
        if (other is SphereCollider) {
            return;
        }
        if ((other.CompareTag("Player") && this.gameObject.CompareTag("Enemy")) ||
            (other.CompareTag("Enemy") && this.gameObject.CompareTag("Player"))
            ) {
//            other.gameObject.GetComponent<HealthManager>().AffectDamage(damage);
//            selfManager.AffectDamage(selfDamage);
        }
    }

    void OnCollisionEnter(Collision collision) {
        //Debug.Log(collision.gameObject.tag);
        if(collision.gameObject.tag == "Player"){
            //Debug.Log("Normal Point1:::"+collision.contacts[0].normal);
            //Debug.Log("Normal Point2:::" + collision.contacts[1].normal);
            //Debug.Log("Point1:::" + collision.contacts[0].point.ToString());
            //Debug.Log("Point2:::" + collision.contacts[1].point.ToString());
            Debug.DrawRay(collision.contacts[0].point, collision.contacts[0].normal, Color.green,500);
            Debug.DrawRay(collision.contacts[1].point, collision.contacts[1].normal, Color.cyan,500);
            collision.gameObject.GetComponent<HealthManager>().AffectDamage(damage);
            selfManager.AffectDamage(selfDamage);
        }
    }
}
