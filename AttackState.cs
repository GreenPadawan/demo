﻿using UnityEngine;
using System.Collections;

public class AttackState : EnemyState {
    private int nextWayPoint;
    //Constructor
    public AttackState(EnemyStateManager stateManger, string name)
    {
        enemyStateManager = stateManger;
        stateName = name;
    }

    public override void PerformAction()
    {
        Chase();
        Attack();
    }

    //Check for the existence of target object in the view
    private void Attack()
    {
//        Vector3 enemyToTarget = (enemyStateManager.chaseTarget.position + enemyStateManager.offset) - enemyStateManager.origin.transform.position;
		Vector3 enemyToTarget = (enemyStateManager.chaseTarget + enemyStateManager.offset) - enemyStateManager.origin.transform.position;
		RaycastHit hit = enemyStateManager.detector.Shootrays(enemyToTarget);
        if (hit.transform != null)
        {
            if (hit.collider.tag.Equals("Player"))
            {
                Debug.DrawRay(hit.transform.position, enemyToTarget, Color.magenta, 500);
//                enemyStateManager.chaseTarget = hit.transform;
				enemyStateManager.chaseTarget = hit.transform.position;
            }
            else
            {
                MoveToState(enemyStateManager.GetAlertState());
            }
        }
        else
        {
            MoveToState(enemyStateManager.GetAlertState());
        }
    }

    private void Chase()
    {
//        enemyStateManager.navMesgAgent.destination = enemyStateManager.chaseTarget.position;
		enemyStateManager.navMesgAgent.destination = enemyStateManager.chaseTarget;
        enemyStateManager.navMesgAgent.Resume();
    }
}
