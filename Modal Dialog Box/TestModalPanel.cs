﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;

public class TestModalPanel : MonoBehaviour {

    public Sprite icon;
    public Sprite backgroundImage;
    public Transform spawnPoint;
    public GameObject go;

    private ModalPanelManager modalPanelManager;

    private UnityAction yesAction;
    private UnityAction noAction;
    private UnityAction cancelAction;


    void Awake() {
        modalPanelManager = ModalPanelManager.GetInstance();
        yesAction = new UnityAction(TestYesFunction);
        noAction = new UnityAction(TestNoFunction);
        cancelAction = new UnityAction(TestCancelFunction);
    }

    void TestYesFunction() {
        Debug.Log("Yes Button Clicked");
    }

    void TestNoFunction()
    {
        Debug.Log("No Button Clicked");
    }

    void TestCancelFunction()
    {
        Debug.Log("Cancel Button Clicked");
    }

    public void CreateModalPanel() {
        //modalPanelManager.Choice("Do you want to exit?", yesAction, noAction, cancelAction);
        modalPanelManager.CreateMessageBox("Number1", "Hello");
    }

    public void CreateModalPanelImage()
    {
        //modalPanelManager.Choice("With the image",  yesAction, noAction, cancelAction, icon);
    }

    public void TestLambda() {
        //modalPanelManager.Choice("Do you want to instantiate?", () => { CreateInstance(go); }, noAction, cancelAction);
        modalPanelManager.CreateMessageBox("Number2", "Hellodasdfa s sd " );
    }

    public void TestGeneric() {
        ButtonDetails button1Details = new ButtonDetails();
        button1Details.buttonLabel = "Yes";
        button1Details.buttonAction = () => { CreateInstance(go); };
        ButtonDetails button2Details = new ButtonDetails();
        button2Details.buttonLabel = "No";
        button2Details.buttonAction = noAction;
        ButtonDetails button3Details = new ButtonDetails();
        button3Details.buttonLabel = "Cancel";
        button3Details.buttonAction = cancelAction;
        
        ModalDialogDetails details = new ModalDialogDetails();
        details.title = "Warning";
        details.question = "Do you really want to exit?";
        details.button1Details = button1Details;
        details.button2Details = button2Details;
        details.button3Details = button3Details;
        details.iconImage = icon;
        modalPanelManager.CreateModalPanel(details);
    }

    public void TestGeneric1()
    {
        //ButtonDetails button1Details = new ButtonDetails();
        //button1Details.buttonLabel = "Yes";
        //button1Details.buttonAction = () => { CreateInstance(go); };
        //ButtonDetails button2Details = new ButtonDetails();
        //button2Details.buttonLabel = "No";
        //button2Details.buttonAction = noAction;
        //ButtonDetails button3Details = new ButtonDetails();

        //ModalDialogDetails details = new ModalDialogDetails();
        //details.title = "Information";
        //details.question = "Do you really want to exit?";
        //details.button1Details = button1Details;
        //details.button2Details = button2Details;
        //modalPanelManager.CreateModalPanel(details);
        //modalPanelManager.CreateMessageBox("Warning", "A warning has been displayed");
        //modalPanelManager.CreateMessageBox("Warning", "A warning has been displayed", yesAction);
        modalPanelManager.CreateMessageBoxWithImages("Warning", "A warning has been displayed", icon, backgroundImage, yesAction);
        //modalPanelManager.CreateExitGameWindow();
    }

    void CreateInstance(GameObject go1)
    {
        Instantiate(go1, spawnPoint.position, spawnPoint.rotation);
        Debug.Log("Instantiated");
    }
}
