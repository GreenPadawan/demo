﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;

public class ModalPanel : MonoBehaviour {

    [SerializeField]
    Text titleText;
    [SerializeField]
    Text questionText;
    [SerializeField]
    Image iconImage;
    [SerializeField]
    Button button1;
    [SerializeField]
    Button button2;
    [SerializeField]
    Button button3;
    [SerializeField]
    Text button1Text;
    [SerializeField]
    Text button2Text;
    [SerializeField]
    Text button3Text;
    [SerializeField]
    Sprite backgroundImage;



    //Constants
    private const string EXIT_CONFIRMATION = "Do you want to quit the game?";
    private const string EXIT_CONFIRMATION_TITLE = "Exit";
    private const string YES = "Yes";
    private const string NO = "No";
    private const string Cancel = "Cancel";
    private const string OK = "Ok";
    private const string EMPTY_STRING = "";

    void Awake() {
        //By Default disable ecerything
        CommonEnabledBehaviour();
    }

    void OnEnable() {
        CommonEnabledBehaviour();
    }

    /*
        Common behaviour when the modal dialog box is enaled
    */
    private void CommonEnabledBehaviour()
    {
        iconImage.gameObject.SetActive(false);
        button1.gameObject.SetActive(false);
        button2.gameObject.SetActive(false);
        button3.gameObject.SetActive(false);
        Image[] childrenImages = gameObject.GetComponentsInChildren<Image>();
        childrenImages[1].sprite = backgroundImage;
    }

    /*
       Quit the application
   */
    private void QuitApplication()
    {
        Application.Quit();
    }

    /*
   Create an exit game popup window.
   If required, then the user can provide an action which needs to be performed
   on click of Yes and No buttons. By default, the game will exit on yes and window will simply close on no.*/
    public void CreateExitGameWindow(UnityAction yesAction = null, UnityAction noAction = null)
    {
        CommonEnabledBehaviour();

        //Create the button 1
        button1.onClick.RemoveAllListeners();
        if (yesAction != null)
            button1.onClick.AddListener(yesAction);
        button1.onClick.AddListener(QuitApplication);
        button1Text.text = YES;
        button1.gameObject.SetActive(true);

        //Create the button 1
        button2.onClick.RemoveAllListeners();
        if (noAction != null)
            button2.onClick.AddListener(noAction);
        button2.onClick.AddListener(ClosePanel);
        button2Text.text = NO;
        button2.gameObject.SetActive(true);

        questionText.text = EXIT_CONFIRMATION;
        titleText.text = EXIT_CONFIRMATION_TITLE;

    }

    /* 
        Create a Modal dialog box with the data provided in the parameters
    */
    public void CreateModalPanel(ModalDialogDetails dialogDetails)
    {
        CommonEnabledBehaviour();

        //Create the button 1
        button1.onClick.RemoveAllListeners();
        button1.onClick.AddListener(dialogDetails.button1Details.buttonAction);
        button1.onClick.AddListener(ClosePanel);
        if (dialogDetails.button1Details.buttonIcon != null)
        {
            button1.GetComponent<Image>().sprite = dialogDetails.button1Details.buttonIcon;
            button1Text.text = EMPTY_STRING;
        }
        else {
            button1Text.text = dialogDetails.button1Details.buttonLabel;
        }
        button1.gameObject.SetActive(true);

        if (dialogDetails.button2Details != null)
        {
            //Create the button 2
            button2.onClick.RemoveAllListeners();
            button2.onClick.AddListener(dialogDetails.button2Details.buttonAction);
            button2.onClick.AddListener(ClosePanel);
            if (dialogDetails.button2Details.buttonIcon != null)
            {
                button2.GetComponent<Image>().sprite = dialogDetails.button2Details.buttonIcon;
                button2Text.text = EMPTY_STRING;
            }
            else {
                button2Text.text = dialogDetails.button2Details.buttonLabel;
            }
            button2.gameObject.SetActive(true);
        }

        if (dialogDetails.button3Details != null)
        {
            //Create the button 3
            button3.onClick.RemoveAllListeners();
            button3.onClick.AddListener(dialogDetails.button3Details.buttonAction);
            button3.onClick.AddListener(ClosePanel);
            if (dialogDetails.button3Details.buttonIcon != null)
            {
                button3.GetComponent<Image>().sprite = dialogDetails.button3Details.buttonIcon;
                button3Text.text = EMPTY_STRING;
            }
            else {
                button3Text.text = dialogDetails.button3Details.buttonLabel;
            }
            button3.gameObject.SetActive(true);
        }


        questionText.text = dialogDetails.question;
        titleText.text = dialogDetails.title;

        //Create the icon Image
        if (dialogDetails.iconImage != null)
        {
            iconImage.sprite = dialogDetails.iconImage;
            iconImage.gameObject.SetActive(true);
        }

        //Add the backgroung Image
        if (dialogDetails.backgroundImage != null)
        {
            Image[] childrenImages = gameObject.GetComponentsInChildren<Image>();
            childrenImages[1].sprite = dialogDetails.backgroundImage;
            //modalPanelObject.GetComponentInChildren<Image>().sprite = dialogDetails.backgroundImage;
        }


    }

   
    /*
    Create a message box with the given message.
    If required, then the user can provide an action which needs to be performed
    on click of Ok. By default, the message box will simply close.*/
    public void CreateMessageBox(string title, string message, UnityAction okAction = null)
    {
        CommonEnabledBehaviour();

        //Create the button 1
        button1.onClick.RemoveAllListeners();
        if (okAction != null)
            button1.onClick.AddListener(okAction);
        button1.onClick.AddListener(ClosePanel);
        button1Text.text = OK;
        button1.gameObject.SetActive(true);

        questionText.text = message;
        titleText.text = title;

    }


    /*
    Create a message box with the given message.
    If required, then the user can provide an action which needs to be performed
    on click of Ok. By default, the message box will simply close.*/
    public void CreateMessageBoxWithImages(string title, string message, Sprite icon, Sprite backgroundImage, UnityAction okAction = null)
    {
        CommonEnabledBehaviour();

        //Create the button 1
        button1.onClick.RemoveAllListeners();
        if (okAction != null)
            button1.onClick.AddListener(okAction);
        button1.onClick.AddListener(ClosePanel);
        button1Text.text = OK;
        button1.gameObject.SetActive(true);

        iconImage.sprite = icon;
        iconImage.gameObject.SetActive(true);
        Image[] childrenImages = gameObject.GetComponentsInChildren<Image>();
        childrenImages[1].sprite = backgroundImage;
        //modalPanelObject.GetComponentInChildren<Image>().sprite = backgroundImage;

        questionText.text = message;
        titleText.text = title;
    }

    void ClosePanel()
    {
        GameObjectUtil.Destroy(this.gameObject);
    }

}
