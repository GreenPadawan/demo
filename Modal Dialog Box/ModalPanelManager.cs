﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class ButtonDetails {
    public string buttonLabel;
    public Sprite buttonIcon;
    public UnityAction buttonAction;
}

public class ModalDialogDetails {
    public string question;
    public string title;
    public Sprite backgroundImage;
    public Sprite iconImage;
    public ButtonDetails button1Details;
    public ButtonDetails button2Details;
    public ButtonDetails button3Details;
}


public class ModalPanelManager : MonoBehaviour {
    [SerializeField]
    GameObject modalPanelObject;
    [SerializeField]
    Canvas canvas;

    private ModalPanel modalPanel;
    private static ModalPanelManager modalPanelManager;

    //Constants
    private const string EXIT_CONFIRMATION = "Do you want to quit the game?";
    private const string EXIT_CONFIRMATION_TITLE = "Exit";
    private const string YES = "Yes";
    private const string NO = "No";
    private const string Cancel = "Cancel";
    private const string OK = "Ok";
    private const string EMPTY_STRING = "";

    public static ModalPanelManager GetInstance() {
        if (!modalPanelManager)
        {
            modalPanelManager = FindObjectOfType(typeof(ModalPanelManager)) as ModalPanelManager;
            if (!modalPanelManager) {
                Debug.LogError("Modal Panel not found in the scene");
            }
        }
        return modalPanelManager;
    }

    public void test() {
        SceneManager.LoadSceneAsync(0);
    }

    /* 
        Create a Modal dialog box with the data provided in the parameters
    */
    public void CreateModalPanel(ModalDialogDetails dialogDetails) {
        GameObjectUtil.InstantiateUI(modalPanelObject, canvas.gameObject).GetComponent<ModalPanel>().CreateModalPanel(dialogDetails);
    }

    /*
   Create an exit game popup window.
   If required, then the user can provide an action which needs to be performed
   on click of Yes and No buttons. By default, the game will exit on yes and window will simply close on no.*/
    public void CreateExitGameWindow(UnityAction yesAction = null, UnityAction noAction = null)
    {
		GameObjectUtil.InstantiateUI(modalPanelObject, canvas.gameObject).GetComponent<ModalPanel>().CreateExitGameWindow(yesAction,noAction);
	}

   

    /*
    Create a message box with the given message.
    If required, then the user can provide an action which needs to be performed
    on click of Ok. By default, the message box will simply close.*/
    public void CreateMessageBox(string title, string message,UnityAction okAction = null) {
        GameObjectUtil.InstantiateUI(modalPanelObject, canvas.gameObject).GetComponent<ModalPanel>().CreateMessageBox(title, message, okAction);
    }


    /*
    Create a message box with the given message.
    If required, then the user can provide an action which needs to be performed
    on click of Ok. By default, the message box will simply close.*/
    public void CreateMessageBoxWithImages(string title, string message, Sprite icon, Sprite backgroundImage, UnityAction okAction = null)
    {
        GameObjectUtil.InstantiateUI(modalPanelObject, canvas.gameObject).GetComponent<ModalPanel>().CreateMessageBoxWithImages(title, message, icon, backgroundImage, okAction);
    }
}
