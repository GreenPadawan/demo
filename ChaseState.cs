﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChaseState : EnemyState {

    private int nearestWaypoints = 3;
	private float shellfireRate = 1.5f;
	private float gunfireRate = 0.2f;
	private float timer;
	private int chaseOrEvade;
	private bool shoot;
	private bool turnTowardsPlayer;
	private RaycastHit playerHit;

    //Constructor
    public ChaseState(EnemyStateManager stateManger,string name)
    {
        enemyStateManager = stateManger;
        stateName = name;
    }

    public override void PerformAction()
    {
        Look();
        Chase();

    }

	public override void PerformPhysicsAction(){
		if(shoot){
			shoot = false;
			Shoot ();
		}
//		if(turnTowardsPlayer){
//			turnTowardsPlayer = false;
//			enemyStateManager.body.MoveRotation (Quaternion.Slerp(enemyStateManager.body.rotation, Quaternion.LookRotation(playerHit.transform.position), 2));
//		}
	}

    //Check for the existence of target object in the view
    private void Look()
    {
//        Vector3 enemyToTarget = (enemyStateManager.chaseTarget.position + enemyStateManager.offset) - enemyStateManager.origin.transform.position;
		Vector3 enemyToTarget = (enemyStateManager.chaseTarget + enemyStateManager.offset) - enemyStateManager.origin.transform.position;
        RaycastHit hit = enemyStateManager.detector.Shootrays(enemyToTarget);
        //Debug.Log("Hit target:::" + hit.collider.tag);
        if (hit.transform != null)
        {
            if (hit.collider.tag.Equals("Player"))
            {
                Debug.DrawRay(hit.transform.position, enemyToTarget, Color.magenta, 500);
//				turnTowardsPlayer = true;
//				enemyStateManager.TurnTowardsPlayer (true, hit.transform.position);
				playerHit = hit;
				timer += Time.deltaTime;
				//		if(timer > gunfireRate){
				if(timer > shellfireRate){
					shoot = true;
				}
//				ChaseOrEvade (hit.transform.position);
//				Debug.Log ("Chase or Evade:::"+ chaseOrEvade);
//				if (chaseOrEvade == -1) {
					if (hit.collider.gameObject.GetComponent<Movement> ().GetBoosted () && !enemyStateManager.GetBoosted ()) {
						enemyStateManager.chaseTarget = hit.transform.position;					
					} else {
						enemyStateManager.chaseTarget = FindIntercetionPoint (hit);
					}
//				} else {
//					MoveToState(enemyStateManager.GetEvadeState());
//				}

				//MoveToState(enemyStateManager.GetAttackState());
            }
            else
            {
//				enemyStateManager.TurnTowardsPlayer (false,Vector3.zero);
                UpdateEffectiveWaypoints();
                //Debug.Log("eff count:::"+ enemyStateManager.effectiveWaypoints.Count);
                MoveToState(enemyStateManager.GetAlertState());

            }
        }
        else
        {
            UpdateEffectiveWaypoints();
			//Debug.Log("eff count1:::" + enemyStateManager.effectiveWaypoints.Count);
//			enemyStateManager.TurnTowardsPlayer (false,Vector3.zero);
            MoveToState(enemyStateManager.GetAlertState());
        }
    }

	private void Shoot(){
		timer = 0;
		enemyStateManager.shooter.Shoot (enemyStateManager.shellSpeed);
//		enemyStateManager.muzzle.Shoot ();
	}

    private void Chase() {
//        enemyStateManager.navMesgAgent.destination = enemyStateManager.chaseTarget.position;
		enemyStateManager.navMesgAgent.destination = enemyStateManager.chaseTarget;
		enemyStateManager.navMesgAgent.Resume();
    }

	private Vector3 FindIntercetionPoint(RaycastHit hit){
		Vector3 targetVelocity = hit.rigidbody.velocity;
		Vector3 targetPosition = hit.transform.position;

		Vector3 currentVelocity = enemyStateManager.gameObject.GetComponent<Rigidbody> ().velocity;
		Vector3 currentPosition = enemyStateManager.gameObject.transform.position;

//		Debug.Log ("CurrentVelocity::"+ currentVelocity);
//		Debug.Log ("CurrentPos::"+ currentPosition);
//		Debug.Log ("TargetVelocity::"+ targetVelocity);
//		Debug.Log ("TarferPos::"+ targetPosition);

		Vector3 relativeVelocity = targetVelocity - currentVelocity;
		Vector3 relativePosition = targetPosition - currentPosition;
//		Debug.Log ("RelVelocity::"+ relativeVelocity);
//		Debug.Log ("RelPos::"+ relativePosition);

		float eta = relativePosition.magnitude / relativeVelocity.magnitude;
//		Debug.Log ("ETA::"+ eta);

		Vector3 interceptPosition = targetPosition + targetVelocity * eta;
//		Transform interceptTransform = new Transform();
//		interceptTransform.position = interceptPosition;
//		interceptTransform.rotation = hit.transform.rotation;
//		interceptTransform.localScale = hit.transform.localScale;

//		Debug.Log ("IPOS::"+ interceptPosition);
		return interceptPosition;
	}

    private void UpdateEffectiveWaypoints() {
        List<float> distanceList = new List<float>(enemyStateManager.waypoints.Length);
        Dictionary<Transform, float> waypointDistance = new Dictionary<Transform, float>();

        foreach(Transform t in enemyStateManager.waypoints) {
//            float d =  Vector3.Distance(enemyStateManager.chaseTarget.position, t.position);
			float d =  Vector3.Distance(enemyStateManager.chaseTarget, t.position);
            distanceList.Add(d);
            
            waypointDistance.Add(t,d);
        }
        distanceList.Sort();
        enemyStateManager.effectiveWaypoints = new List<Transform>(nearestWaypoints);
        for (int j = 0; j < nearestWaypoints; j++) {
            foreach (Transform t in enemyStateManager.waypoints) {
                if (waypointDistance[t] == distanceList[j] && distanceList.IndexOf(waypointDistance[t]) < nearestWaypoints) {
                    enemyStateManager.effectiveWaypoints.Add(t);
                }
            }
        }
    }

	private void ChaseOrEvade(Vector3 pos){
		PotentialFunction potentialValues = enemyStateManager.potentialValues;
		float r = (pos - enemyStateManager.transform.position).magnitude;
		float U = potentialValues.B / Mathf.Pow (r, potentialValues.m) - potentialValues.A / Mathf.Pow (r, potentialValues.n);
		chaseOrEvade = (int)Mathf.Sign (U);
	}
}
