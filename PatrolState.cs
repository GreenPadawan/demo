﻿using UnityEngine;
using System.Collections;


public class PatrolState : EnemyState
{
    private int nextWayPoint;
    private float maxSearchTime = 10;
    private float searchTime;
    //Constructor
    public PatrolState(EnemyStateManager stateManger, string name) {
        enemyStateManager = stateManger;
        stateName = name;
    }

    public override void PerformAction() {
        Look();
        Patrol();
//		CheckAmmenities ();
    }

	//Check if any ammenities are currently up for grab or not
	private void CheckAmmenities(){
//		GameObject []objects = GameObject.FindGameObjectsWithTag ("Ammenities");
////		Debug.Log ("Length:::" + objects.Length);
//		if(objects.Length == 0){
//			return;
//		}
//		GameObject obj = objects[Random.Range(0,objects.Length)];
//		enemyStateManager.SetAmmenity (obj);
////		MoveToState(enemyStateManager.GetAmmenityState());
	}

    //Check for the existence of target object in the view
	private void Look() {
        RaycastHit hit = enemyStateManager.detector.Shootrays();
        if (hit.transform != null) {
            MoveToState(enemyStateManager.GetChaseState());
//            enemyStateManager.chaseTarget = hit.transform;
			enemyStateManager.chaseTarget = hit.transform.position;
        }
    }


    // Move towards the designated waypoints
    private void Patrol()
    {
        /*
        if the enemy goes from patrol->chase, then the effective waypoints are recalculated.
        Then, when coming back to the Patrol via Alert state, there is a possibility that the
        next Waypoint doesn't exist in the current list, therefore recalculate it.
        */
        if (nextWayPoint >= enemyStateManager.effectiveWaypoints.Count) {
            nextWayPoint = Random.Range(0, enemyStateManager.effectiveWaypoints.Count);
        }

        try {
            enemyStateManager.navMesgAgent.destination = enemyStateManager.effectiveWaypoints[nextWayPoint].position;
        }
        catch (System.ArgumentOutOfRangeException) {
            Debug.Log("Exception::: next waypoint::::"+nextWayPoint);
            Debug.Log("Exception::: waypoints::::" + enemyStateManager.effectiveWaypoints.Count);
            nextWayPoint = 0;
		}
//		Debug.Log ("Count::"+ enemyStateManager.effectiveWaypoints.Count);
//		Debug.Log ("Next::"+ enemyStateManager.effectiveWaypoints[nextWayPoint].name);
        enemyStateManager.navMesgAgent.Resume();
        if (enemyStateManager.navMesgAgent.remainingDistance <= enemyStateManager.navMesgAgent.stoppingDistance
            && !enemyStateManager.navMesgAgent.pathPending) {
            nextWayPoint = Random.Range(0, enemyStateManager.effectiveWaypoints.Count);
            searchTime += Time.time;
            //Debug.Log("Effective Waypoints length" + enemyStateManager.effectiveWaypoints.Count);
            if (searchTime > maxSearchTime)
            {
                searchTime = 0;
                if (enemyStateManager.effectiveWaypoints.Count != enemyStateManager.waypoints.Length)
                {
                    enemyStateManager.SetEffectiveWaypoints();
                }
            }
        }
    }
}
