﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpeedUpBox : MonoBehaviour {

	public int boostValue;
	public Slider valueSlider;

	void Start () {
		boostValue = 50;
	}

	void OnEnable(){
		boostValue = 50;
		gameObject.GetComponent<Rigidbody> ().useGravity = true;
	}

	void Update(){
		valueSlider.value = boostValue;
		if(boostValue <= 0){
			GameObjectUtil.Destroy (gameObject);
		}
	}

	void OnTriggerEnter(Collider other){
		if(other.CompareTag("Floor")){
			gameObject.GetComponent<Rigidbody> ().velocity = Vector3.zero;
			gameObject.GetComponent<Rigidbody> ().useGravity = false;
			StartCoroutine (DecrementValue());
		}
		else if(other.CompareTag("Player")){
			other.gameObject.GetComponent<Movement>().AddBoost(boostValue);
			GameObjectUtil.Destroy (gameObject);
		}else if(other.CompareTag("Enemy")){
			if(other.GetType ().ToString()=="UnityEngine.SphereCollider"){
				return;
			}
			other.gameObject.GetComponent<EnemyStateManager>().AddBoost(boostValue) ;
			other.gameObject.GetComponent<EnemyStateManager> ().SetAmmenity (null);
			GameObjectUtil.Destroy (gameObject);
		}
	}

	IEnumerator DecrementValue(){
		while(true){
			boostValue -= 5;
			yield return new WaitForSeconds(1);
		}
	}
}
