﻿using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour {

	private AudioSource source;
	private bool explode;
	public float blastRadius = 1;
	public GameObject shellHitEffectPrefab;

	void Awake(){
		source = GetComponent<AudioSource> ();
		explode = false;
	}

	void OnEnable(){
		explode = false;
	}

	void OnCollisionEnter(Collision collision){
		if(collision.collider.CompareTag("Floor")){
			source.Play ();
			GameObjectUtil.Destroy (gameObject);
		}

		if(collision.collider.CompareTag("Enemy")){
			collision.collider.gameObject.GetComponent<HealthManager> ().AffectDamage (20);
			source.Play ();
			explode = true;
//			GameObjectUtil.Destroy (gameObject);
		}

		if(collision.collider.CompareTag("Player")){
			collision.collider.gameObject.GetComponent<HealthManager> ().AffectDamage (20);
			source.Play ();
			explode = true;
//			GameObjectUtil.Destroy (gameObject);
		}

		if(collision.collider.CompareTag("Shot")){
			source.Play ();
			GameObjectUtil.Destroy (gameObject);
		}

		if(collision.collider.CompareTag("Ammenities")){
			return;
		}
	}

	void FixedUpdate(){
		if(explode){
			Collider[] hitColliders = Physics.OverlapSphere (transform.position, blastRadius);
			for (int i = 0; i < hitColliders.Length; i++) 
			{
//				Debug.Log (hitColliders[i].tag+":::::"+hitColliders[i].GetType().ToString());
				if(hitColliders[i].GetType().ToString().Equals("UnityEngine.SphereCollider")){
					continue;
				}
				if (hitColliders [i].GetComponent<Rigidbody> () != null)
				{
					hitColliders [i].GetComponent<Rigidbody> ().AddExplosionForce (90, transform.position, blastRadius);
					GameObject obj = GameObjectUtil.Instantiate (shellHitEffectPrefab,hitColliders [i].transform.position);
					obj.transform.position = hitColliders [i].transform.position;
				}
			}
			GameObjectUtil.Destroy (gameObject);
		}
	}
}
