﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShellBox : MonoBehaviour {

	public int shells;
	public float value;
	public int maxShells = 10;
	public float maxTime = 15;
	public Slider valueSlider;
	public Slider shellSlider;

	void Start () {
		shells = maxShells;
		value = maxTime;
	}

	void OnEnable(){
		shells = maxShells;
		value = maxTime;
		gameObject.GetComponent<Rigidbody> ().useGravity = true;
	}

	void Update(){
		valueSlider.value = value;
		shellSlider.value = shells;
		if(shells <= 0 || value <= 0){
			GameObjectUtil.Destroy (gameObject);
		}
	}

	void OnTriggerEnter(Collider other){
		if(other.CompareTag("Floor")){
			gameObject.GetComponent<Rigidbody> ().velocity = Vector3.zero;
			gameObject.GetComponent<Rigidbody> ().useGravity = false;
			StartCoroutine (DecrementValue());
		}
		else if(other.CompareTag("Enemy") || other.CompareTag("Player")){
			if(other.GetType ().ToString()=="UnityEngine.SphereCollider"){
				return;
			}
			shells = other.gameObject.GetComponentInChildren<ShootShell>().AddShells(shells);
		}
	}

	IEnumerator DecrementValue(){
		while(true){
			value -= 1;
			yield return new WaitForSeconds(1);
		}
	}
}
