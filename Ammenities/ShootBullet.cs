﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShootBullet : MonoBehaviour {

	public int damageValue;
	public int maxDistance;
	public float fireRate = 0.2f;
	public int bullets;
	public int maxBullets;
	public GameObject bulletHitEffectPrefab;
	public Text bulletText;

	private AudioSource source;
	private Ray ray;
	private RaycastHit hit;
	private float timer;
	private GameManager gameManager;
	private Light muzzleFlashLight;

	void Awake(){
		gameManager = GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager>();
		timer = 0;
		source = GetComponent<AudioSource> ();
		muzzleFlashLight = GetComponent<Light> ();
		bulletText.text = bulletText.text.Insert (bulletText.text.IndexOf (":")+1,bullets.ToString());
	}

	void Update(){
		timer += Time.deltaTime;
		float fire = Input.GetAxis("Fire1");
		if( fire !=0 && CompareTag("Player") && !gameManager.IsMobile()){
			if (timer > fireRate) {
				timer = 0;
				Shoot ();
			}
		}
		bulletText.text = bulletText.text.Remove (bulletText.text.IndexOf (":")+1);
		bulletText.text = bulletText.text.Insert (bulletText.text.IndexOf (":")+1,bullets.ToString());
	}

	public void Shoot(){
		if(bullets <= 0){
			return;
		}
		ray.origin = transform.position;
		ray.direction = transform.forward;
		Physics.Raycast (ray,out hit,maxDistance);
		source.Play ();
		StartCoroutine (ShowMuzzleFlash());
		if(hit.transform != null){
			if (hit.collider.CompareTag ("Player") || hit.collider.CompareTag ("Enemy")) {
				GameObjectUtil.Instantiate (bulletHitEffectPrefab, hit.transform.position);
				hit.collider.gameObject.GetComponent<HealthManager> ().AffectDamage (damageValue);
			}
		}
		bullets--;
		StopCoroutine (ShowMuzzleFlash());
	}

	public int AddBullets(int count){
		int c = count <= maxBullets - bullets ? count : maxBullets - bullets;
		bullets += c;
		return count - c;
	}

	IEnumerator ShowMuzzleFlash(){
		muzzleFlashLight.enabled = true;
		yield return new WaitForSeconds (0.05f);
		muzzleFlashLight.enabled = false;
	}
}
