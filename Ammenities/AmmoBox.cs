﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AmmoBox : MonoBehaviour {

	public int ammo;
	public float value;
	public int maxAmmo = 10;
	public float maxTime = 15;
	public Slider valueSlider;
	public Slider ammoSlider;

	void Start () {
		ammo = maxAmmo;
		value = maxTime;
	}

	void OnEnable(){
		ammo = maxAmmo;
		value = maxTime;
		gameObject.GetComponent<Rigidbody> ().useGravity = true;
	}

	void Update(){
		valueSlider.value = value;
		ammoSlider.value = ammo;
		if(ammo <= 0 || value <= 0){
			GameObjectUtil.Destroy (gameObject);
		}
	}

	void OnTriggerEnter(Collider other){
		if(other.CompareTag("Floor")){
			gameObject.GetComponent<Rigidbody> ().velocity = Vector3.zero;
			gameObject.GetComponent<Rigidbody> ().useGravity = false;
			StartCoroutine (DecrementValue());
		}
		else if(other.CompareTag("Enemy") || other.CompareTag("Player")){
			if(other.GetType ().ToString()=="UnityEngine.SphereCollider"){
				return;
			}
			ammo = other.gameObject.GetComponentInChildren<ShootBullet>().AddBullets(ammo);
		}
	}

	IEnumerator DecrementValue(){
		while(true){
			value -= 1;
			yield return new WaitForSeconds(1);
		}
	}
}
