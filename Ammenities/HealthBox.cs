﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthBox : MonoBehaviour {

	public int value;
	public Slider valueSlider;

	void Start () {
		value = 100;
	}

	void OnEnable(){
		value = 100;
		gameObject.GetComponent<Rigidbody> ().useGravity = true;
	}

	void Update(){
		valueSlider.value = value;
		if(value <= 0){
			GameObjectUtil.Destroy (gameObject);
		}
	}

	void OnTriggerEnter(Collider other){
		if(other.CompareTag("Floor")){
			gameObject.GetComponent<Rigidbody> ().velocity = Vector3.zero;
			gameObject.GetComponent<Rigidbody> ().useGravity = false;
			StartCoroutine (DecrementValue());
		}
		else if(other.CompareTag("Enemy") || other.CompareTag("Player")){
			if(other.GetType ().ToString()=="UnityEngine.SphereCollider"){
				return;
			}
			other.gameObject.GetComponent<HealthManager>().RegenerateHealth(value);
			GameObjectUtil.Destroy (gameObject);
		}
	}

	IEnumerator DecrementValue(){
		while(true){
			value -= 5;
			yield return new WaitForSeconds(1);
		}
	}
}
